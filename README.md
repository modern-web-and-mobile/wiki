# [Wiki Home Page](https://bitbucket.org/modern-web-and-mobile/wiki/wiki/Home)
### What is this repository for? ###

This repository will serve as the primary starting point for modern web and mobile developer guides and links.

### How can I contribute? ###

Clone the repo, create a branch for a guide you'd like to add, push the guide, and create a PR!

### Who do I talk to? ###

[Jason Ransom](mailto:jason.ransom@slalom.com)